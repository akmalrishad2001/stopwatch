import React, { useState, useEffect } from "react";
import "../css/stopwatch.css";

export const Stopwatch = () => {
  const [time, setTime] = useState(0);
  const [isRunning, setIsRunning] = useState(false);

  useEffect(() => {
    let intervalId;
    if (isRunning) {
      // setting time from 0 to 1 every 10 milisecond using javascript setInterval method
      intervalId = setInterval(() => setTime(time + 1), 10);
    }
    return () => clearInterval(intervalId);
  }, [isRunning, time]);

  const hours = Math.floor(time / 360000);

  const minutes = Math.floor((time % 360000) / 6000);

  const second = Math.floor((time % 6000) / 100);

  const startHandler = () => {
    setIsRunning(true);
  };
  const stopHandler = () => {
    setIsRunning(false);
  };
  const resetHandler = () => {
    setTime(0);
  };

  return (
    <div>
      <div className="stopwatch-container">
        <p>
          {hours}:{minutes.toString()}:{second.toString()}
        </p>
        <p>jam:menit:detik</p>
      </div>
      <div className="button-container">
        <button onClick={resetHandler} style={{ backgroundColor: "yellow" }}>
          Reset
        </button>
        <button onClick={startHandler} style={{ backgroundColor: "green" }}>
          Start
        </button>
        <button onClick={stopHandler} style={{ backgroundColor: "red" }}>
          Stop
        </button>
      </div>
    </div>
  );
};
