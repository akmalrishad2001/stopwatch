import React from "react";
import { Stopwatch } from "../assets/components/Stopwatch";
import "../assets/css/global.css";

export const App = () => {
  return (
    <div className="container">
      <Stopwatch></Stopwatch>
    </div>
  );
};
